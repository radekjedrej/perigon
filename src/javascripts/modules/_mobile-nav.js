import $ from 'jquery'

class MobileNav {
    constructor() {

        this.burgerIcon = $(".nav-bar__mobileIcon")
        this.menu = $(".nav-bar__mobileMenu")
        this.menuLink = $(".nav-bar__mobileListItem")
        this.drop = $(".nav-bar__mobileDropdown")
        this.events()
    }

    events() {
        this.burgerIcon.on("click", this.slideUp.bind(this))
        // this.menuLink.on("click", this.dropdown.bind(this))
        
        this.submenu()
        // console.log(this.drop)
    }

    submenu() {
        const dropDownMenu = this.drop.parent()
        dropDownMenu.addClass("nav-bar__has-child")
        dropDownMenu.on('click', function ToggleSubMenuOpen(e){
            
            if(!$(e.target).is(".nav-bar__mobileDropdownLink")){
                $(this).toggleClass("active")
                $(this).find(".nav-bar__mobileDropdown").slideToggle(200, "swing");
            } else{
                return;
            }
        })
    }

    slideUp() {
        this.burgerIcon.toggleClass("toggle")
        
        this.menu.slideToggle(200, "swing")
    }

    // dropdown(e) {
    //     $(this).parent().siblings().find(".nav-bar__mobileDropdown").slideToggle("slow", "swing");
    // }
}

export default MobileNav