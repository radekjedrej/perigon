export default class DeviceCheck {

    constructor(device = 'mobile') {
        this.device = device === 'mobile' ? 767 : 1279
        this.onCreate()
    }

    get check() {
        return this.isMobile
    }

    checkDevice() {
        const { 
            matches: mobile 
        } = window.matchMedia(`(max-width: ${this.device}px)`)

        return mobile
    }

    onCreate() {
        const that = this; {
        that.isMobile = that.checkDevice()

        window.addEventListener('resize', () => setTimeout(() => {
            that.isMobile = that.checkDevice()
        }, 200))
    }}
}