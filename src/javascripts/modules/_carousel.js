import $ from 'jquery';
import Slick from 'slick-carousel';

class Carousel {
    constructor() {

        this.init()
    }

    init() {

        $('.js-carousel').slick({
            dots: true,
            arrows: true,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            prevArrow:"<div class='carousel-row__prev slick-prev'><img class='carousel-row__arrow' src='assets/images/svg/ChevronLeft.svg'></div>",
            nextArrow:"<div class='carousel-row__next slick-next'><img class='carousel-row__arrow' src='assets/images/svg/ChevronRight.svg'></div>",
        });
    }
}

export default Carousel;