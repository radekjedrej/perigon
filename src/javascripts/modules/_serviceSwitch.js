import $ from 'jquery'

class Switch {
    constructor(){
        this.link = $('.services-row__link')
        this.firstLink = $('.services-row__link:first')
        this.firstContentBox = $('.services-row__contentBox:first')
        this.contentBox = $('.services-row__contentBox')

        this.firstLink.addClass('active');
        this.contentBox.hide();
        this.firstContentBox.show();
        this.events()
    }
    
    events(){
        this.link.on("click", this.changeContent)
    }

    changeContent(){
        $('.services-row__link').removeClass('active');
        $(this).addClass('active');
        $('.services-row__contentBox').hide();

        let activeTab = $(this).attr('href');
        $(activeTab).fadeIn();
        return false;
    }   
}

export default Switch