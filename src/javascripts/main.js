import { handleWindow } from './modules/_helpers'
import DeviceCheck from './modules/_deviceChecjk'
import scrollr from 'skrollr';
import LazyLoad from 'vanilla-lazyload';
import Carousel from './modules/_carousel';
import Switch from './modules/_serviceSwitch';
import MobileNav from './modules/_mobile-nav';


window.addEventListener('DOMContentLoaded', () => {
    // Variables
    const isMobile = new DeviceCheck('desktop')

    // Object Instance
    const myLazyLoad = new LazyLoad();

    if (!isMobile.check) {
        let s = scrollr.init()
    }

    new Carousel();
    new Switch();
    new handleWindow();
    new MobileNav();

    // Show the first tab and hide the rest
    
})

